<?php
namespace soap\utils;

class tabla {// clase para crear una tabla automaticamente 
    private $campos=array();
    private $datos = array();


    public function __construct(array $campos,array $datos)// se manda a llamar al modelo de la BD
	{
        $this->campos=$campos;
        $this->datos=$datos;
	}
        
        
    public function dibuja_tabla(){// se recive el nombre de la tabla
       $campos= $this->campos;
       $datos=$this->datos;
        $html = "";// se declara un string llamado html donde se guardara el codigo html
        $html .= '
        <table class="table table-bordered">
              <thead>
                <tr>';//codigo html para la tabla
                 
                  //$consulta = $sn->campos($tabla);// se consultas los campos de la tabla
        $cont=0;
                  while (count($campos)) {// se crea un while para obetener los datos 
                     $html .= "<th>".$campos[$cont]."</th>";// se añade los campos al html
                     $cont++;
                  }
                  
                                   
               $html .=' </tr>
              </thead>
              <tbody>';//codigo html
                
               $html .= "<tr>";
               $cont=0;
                 while (count($datos)) {// se crea un while para obetener los datos 
                     $html .= "<th>".$datos[$cont]."</th>";// se añade los campos al html
                     $cont++;
                  }
                  
              
              $html .=' </tbody>
            </table>';//codigo html
        
        return $html;//retorna el string html con la estructura y datos de la tabla ya creados
    }
	
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
