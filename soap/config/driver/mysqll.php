<?php
namespace soap\config\drivers;
use mysqli;
//include_once 'IDriverCoreQueryStrategy.php';
class mysqll {
    private $mysqli;
    private $mysqli_sn;
    public function __construct(array $databases){
        // se realiza la coneccion
        $mysqli = new mysqli($databases['default']['host'], $databases['default']['user'], $databases['default']['password'], $databases['default']['db']);
        if ($mysqli->connect_errno) {
              echo "Falló la conexión con MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
           }else{
            //   echo 'exito';
           $this->mysqli = $mysqli;// se guarda la coneccion
           }
          
	}
     public function conec_sn(array $databases){
         $mysqli = new mysqli($databases['default']['host'], $databases['default']['sn_user'], $databases['default']['sn_password'], $databases['default']['sn']);
        if ($mysqli->connect_errno) {
              echo "Falló la conexión con MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
           }else{
              
           $this->mysqli_sn = $mysqli;// se guarda la coneccion
           }
         
     }


     
        
   public function get_campos($tabla){//metodo para obtener los campos de una tabla
       $mysqli = $this->mysqli;
       $resultado = $mysqli->query("SHOW COLUMNS FROM ".$tabla);
       return $resultado;
       
   }
   
   public function get_all($tabla){//metodo para realizar la consulta de una tabla
       $mysqli = $this->mysqli;
       $resultado = $mysqli->query("select * from ".$tabla);
       return $resultado;
       
   }
   
   public function query_consulta($sql){
       $mysqli = $this->mysqli_sn;
       $resultado = $mysqli->query($sql);
       return $resultado;
   }
   public function query_consulta_sn($sql){
       $mysqli = $this->mysqli_sn;
       $resultado = $mysqli->query($sql);
       return $resultado;
   }
   
    public function inf_tabla_sn($tabla){
       $mysqli = $this->mysqli_sn;
       $resultado = $mysqli->query("select Servidor,tabla,FH,FV,R,C, 
case 
  When FH = 'S' then 'FH'
  When FV = 'S' then 'FV'
  When R = 'S' then 'R'
  When C = 'S' then 'C'
  END AS Tipo
from tablas where NombreDato = '$tabla' ");
       return $resultado;
   }
   
    public function get_campos_sn($tabla){//metodo para obtener los campos de una tabla
       $mysqli = $this->mysqli_sn;
       $resultado = $mysqli->query("select distinct Nombre as Field from tablas t, campos c where t.idtabla = c.IdTabla and t.NombreDato = '$tabla'");
       return $resultado;
       
   }
   
        
   
}


?>
