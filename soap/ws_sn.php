
<?php
//namespace soap;

include('config/lib/nusoap.php');
include ('model/model_soap_sn.php');



$server = new soap_server();
$server->configureWSDL('Servidor Soap SN', 'urn:Servidor');                            




$server->register('sn_campos',											// method name
    array('tabla' => 'xsd:string'),	// input parameters
    array('return' => 'xsd:Array'),										// output parameters
    'urn:sn_camposwsdl',													// namespace
    'urn:sn_camposwsdl#sn_campos',									// soapaction
    'rpc',																	// style
    'encoded',																// use
    'Retorna los campos de la tabla desde el servidor de nombres'														// documentation
);
$server->register('sn_consulta_datos',											// method name
    array('tabla' => 'xsd:string'),	// input parameters
    array('return' => 'xsd:Array'),										// output parameters
    'urn:sn_consulta_datoswsdl',													// namespace
    'urn:sn_consulta_datoswsdl#sn_consulta_datos',									// soapaction
    'rpc',																	// style
    'encoded',																// use
    'Retorna los campos datos de los distintos servidores soap'														// documentation
);


function sn_campos($tabla){
    $model = new model_soap_sn();
    return $model->sn_campos($tabla);
}
function sn_consulta_datos($tabla){
    $model = new model_soap_sn();
    return $model->sn_datos($tabla);
  
}





$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);

 
