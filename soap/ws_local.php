
<?php
//namespace soap;

include('config/lib/nusoap.php');
include ('model/model_soap.php');



$server = new soap_server();
$server->configureWSDL('Servidor Soap SN', 'urn:Servidor');                            
$server->register('MetodoPrueba',					        // method name
    array('tcParametroA' => 'xsd:string','tcParametroB' => 'xsd:string'),	// input parameters
    array('return' => 'xsd:string'),						// output parameters
    'urn:MetodoPruebawsdl',							// namespace
    'urn:MetodoPruebawsdl#MetodoPrueba',					// soapaction
    'rpc',									// style
    'encoded',									// use
    'Retorna el datos'								// documentation
);

$server->register('Consulta_Campos',											// method name
    array('tabla' => 'xsd:string'),	// input parameters
    array('return' => 'xsd:string'),										// output parameters
    'urn:Consulta_Camposwsdl',													// namespace
    'urn:Consulta_Camposwsdl#Consulta_Campos',									// soapaction
    'rpc',																	// style
    'encoded',																// use
    'Retorna los campos de la tabla'														// documentation
);
$server->register('Consulta_datos',											// method name
    array('tabla' => 'xsd:string'),	// input parameters
    array('return' => 'xsd:Array'),										// output parameters
    'urn:Consulta_datoswsdl',													// namespace
    'urn:Consulta_datoswsdl#Consulta_datos',									// soapaction
    'rpc',																	// style
    'encoded',																// use
    'Retorna los datos de la tabla'														// documentation
);


function Consulta_Campos($tabla) {
         $model = new model_soap();
         
        // $respuesta = $model->campos_tabla($tabla);
        //use app\model\soap\sn\model_soap;
	return $model->campos_tabla($tabla);
}

function Consulta_datos($tabla) {
         $model = new model_soap();
         
        // $respuesta = $model->campos_tabla($tabla);
        //use app\model\soap\sn\model_soap;
	return $model->consulta_datos_local($tabla);
}




$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);

 

