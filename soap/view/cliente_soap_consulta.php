<?php
include $_SERVER["DOCUMENT_ROOT"].'/bibliotecas_soap/soap/config/lib/nusoap.php';
include $_SERVER["DOCUMENT_ROOT"].'/bibliotecas_soap/soap/config/databases.php';





function head(){
    $html="<head>

		<link rel='stylesheet' type='text/css' href='soap/resources/css/bootstrap.css'/>
		</head>";
    return $html;
}
function consulta(){
////////////////// Se obtiene la configuracion ////////////////////////////////////////////////////////////
	$html="";
         $config = new \app\config\databases;//se manda a llamar a la configuracion de la BD que esta en databases.php
         $databases=$config->databases;
         $client = new nusoap_client(''.$databases['default']['ws_sn'].'?wsdl','wsdl');
///////////////////////////////////////////////////////////////////////////////////////////////////////////
        $err = $client->getError();
        if ($err) {
            echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }
        $param = array('tabla' => 'libro');
        $result = $client->call('sn_campos', $param);
        if ($client->fault) {
            echo '<h2>Fault</h2><pre>';
            print_r($result);
	    echo '</pre>';
            }
            else {
                // Check for errors
                $err = $client->getError();
                if ($err) {
                    // Display the error
                    echo '<h2>Error</h2><pre>' . $err . '</pre>';
                    } else {
                        // Display the result
                       
                       $campos = $result;
                        $html.=crea_campos($campos);// se dibuja la tabla
		        //echo $campos[1];
		        
	              }
             }
             
/*                 
 *                     DATOS
 */
        $param2 = array('tabla' => 'libro');
        $result2 = $client->call('sn_consulta_datos', $param2);
        if ($client->fault) {
            echo '<h2>Fault</h2><pre>';
            print_r($result);
	    echo '</pre>';
            }
            else {
                // Check for errors
                $err = $client->getError();
                if ($err) {
                    // Display the error
                    echo '<h2>Error</h2><pre>' . $err . '</pre>';
                    } else {
                        // Display the result
                       
                       $datos = $result2;
                        $html.=crea_datos($datos);// se dibuja la tabla
		        //echo $campos[1];
		        
	              }
             }
  return $html;
}

function crea_campos(array $campos){// metodo para dibujar los campos de la tabla
        $html = "";// se declara un string llamado html donde se guardara el codigo html
        $html .= '
        <table class="table table-bordered">
              <thead>
                <tr>';//codigo html para la tabla
   // return count($campos);
       foreach ($campos as $value) {
              $html .= "<th>".$value."</th>";   
       }
        $html .=' </tr>
              </thead>';
        return $html;
}
function crea_datos(array $datos){
   $html= '<tbody>';
   
   for ($index = 0; $index < count($datos); $index++) {
    $html .= '<tr>';
    
       for ($index2 = 0; $index2 < count($datos[$index]); $index2++) {
           $html .=' <td>'.$datos[$index][$index2].'</td>';
           }
       
  
     $html .='</tr>';
}
              $html .='</table>';   
      return $html;            
                  
             
            
}

?>
